package filters;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import encryption.MD5;
import models.Admin;
import models.Doctor;
import models.Nurse;
import models.Patient;
import service.AdminService;
import service.DoctorService;
import service.NurseService;
import service.PatientService;

public class AuthFilter implements Filter {

	public AuthFilter() {
		// TODO Auto-generated constructor stub
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("utf-8");
		Patient patient = null;
		Doctor doctor = null;
		Nurse nurse = null;
		Admin admin = null;
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String login = httpServletRequest.getParameter("login");
		String password = httpServletRequest.getParameter("password");

		// Filtering Patients
		if (request.getParameter("user").equals("patient")) {
			try {
				patient = PatientService.getPatientByLogin(httpServletRequest.getParameter("login"));
				if (patient == null) {
					httpServletRequest.setAttribute("userLogin", login);
					httpServletRequest.setAttribute("errorMsg", "Wrong login! Try again");
					httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
							httpServletResponse);
				} else if (login.equals(patient.getLogin())) {
					if (MD5.md5(password).equals(patient.getPassword())) {
						request.setAttribute("user", patient);
						chain.doFilter(request, response);
					} else {
						httpServletRequest.setAttribute("userLogin", login);
						httpServletRequest.setAttribute("errorMsg", "Wrong password! Try again!");
						httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
								httpServletResponse);
					}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Filtering Doctors
		else if (request.getParameter("user").equals("doctor")) {
			try {
				doctor = DoctorService.getDoctorByLogin(httpServletRequest.getParameter("login"));
				if (doctor == null) {
					httpServletRequest.setAttribute("userLogin", login);
					httpServletRequest.setAttribute("errorMsg", "Wrong login! Try again");
					httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
							httpServletResponse);
				} else if (login.equals(doctor.getLogin())) {
					if (MD5.md5(password).equals(doctor.getPassword())) {
						request.setAttribute("user", doctor);
						chain.doFilter(request, response);
					} else {
						httpServletRequest.setAttribute("userLogin", login);
						httpServletRequest.setAttribute("errorMsg", "Wrong password! Try again!");
						httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
								httpServletResponse);
					}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Filtering Nurses
		else if (request.getParameter("user").equals("nurse")) {
			try {
				nurse = NurseService.getNurseByLogin(httpServletRequest.getParameter("login"));
				if (nurse == null) {
					httpServletRequest.setAttribute("userLogin", login);
					httpServletRequest.setAttribute("errorMsg", "Wrong login! Try again");
					httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
							httpServletResponse);
				} else if (login.equals(nurse.getLogin())) {
					if (MD5.md5(password).equals(nurse.getPassword())) {
						request.setAttribute("user", nurse);
						chain.doFilter(request, response);
					} else {
						httpServletRequest.setAttribute("userLogin", login);
						httpServletRequest.setAttribute("errorMsg", "Wrong password! Try again!");
						httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
								httpServletResponse);
					}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Filtering Admins
		else if (request.getParameter("user").equals("admin")) {
			try {
				admin = AdminService.getAdminByLogin(httpServletRequest.getParameter("login"));
				if (admin == null) {
					httpServletRequest.setAttribute("userLogin", login);
					httpServletRequest.setAttribute("errorMsg", "Wrong login! Try again");
					httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
							httpServletResponse);
				} else if (login.equals(admin.getLogin())) {
					if (MD5.md5(password).equals(admin.getPassword())) {
						request.setAttribute("user", admin);
						chain.doFilter(request, response);
					} else {
						httpServletRequest.setAttribute("userLogin", login);
						httpServletRequest.setAttribute("errorMsg", "Wrong password! Try again!");
						httpServletRequest.getRequestDispatcher("/login.jsp").forward(httpServletRequest,
								httpServletResponse);
					}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
