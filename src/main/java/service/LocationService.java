package service;

import java.sql.SQLException;
import java.util.Collection;

import dao.LocationDAO;
import models.Location;


public abstract class LocationService {
	public static Collection<Location> getAllLocations() throws ClassNotFoundException, SQLException {
		return LocationDAO.getAllLocations();
	}	
	
	public static void settlePatient(Integer patientId, Integer wardNumber, Integer bedNumber) throws ClassNotFoundException, SQLException {
		LocationDAO.setPatientByNumbers(patientId, wardNumber, bedNumber);
	}
	
	public static void releaseLocation(Integer wardNumber, Integer bedNumber) throws ClassNotFoundException, SQLException {
		LocationDAO.releaseLocation(wardNumber, bedNumber);
	}
	
	public static Location getLocationByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {
		return LocationDAO.getLocationByPatientId(patientId);
	}
}

