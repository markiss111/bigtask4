package service;

import java.sql.SQLException;

import dao.AdminDAO;
import models.Admin;


public abstract class AdminService {
	public static Admin getAdminByLogin(String login) throws ClassNotFoundException, SQLException{
		return AdminDAO.getAdminByLogin(login);
	}	
	
}

