package service;

import java.sql.SQLException;
import java.util.Collection;

import dao.NurseDAO;
import models.Nurse;

public abstract class NurseService {
	
	public static void addNewNurse(String fname, String lname, String surname, String phone, String address, String login, String password,
			String avatar) throws ClassNotFoundException, SQLException {
		NurseDAO.addNewNurse(fname, lname, surname, phone, address, login, password, avatar);
	}
	
	public static Collection<Nurse> getAllNurses() throws ClassNotFoundException, SQLException {
		return NurseDAO.getAllNurses();
	}
	
	public static Nurse getNurseByLogin(String login) throws ClassNotFoundException, SQLException{
		return NurseDAO.getNurseByLogin(login);
	}
	
	public static void setNurseFiredById(Integer nurseId) throws ClassNotFoundException, SQLException {
		NurseDAO.setNurseFiredById(nurseId);
	}
	
	public static Nurse getNurseById(Integer nurseId) throws ClassNotFoundException, SQLException {
		return NurseDAO.getNurseById(nurseId);
	}
	
}

