package service;

import java.sql.SQLException;
import java.util.Collection;

import dao.PatientDAO;
import models.Doctor;
import models.Location;
import models.Patient;

public abstract class PatientService {
	public static void insertNewPatient(String fname, String lname, String surname, String phone, String address, Doctor doctor,
			String diagnosis, String login, String password,
			String avatar) throws SQLException, ClassNotFoundException {
		PatientDAO.addNewPatient(fname, lname, surname, phone, address, doctor, diagnosis, login, password, avatar);
	}
	
	public static Collection<Patient> getPatientsByDoctorId(Integer id) throws SQLException, ClassNotFoundException {
		return PatientDAO.getPatientsByDoctorId(id);
	}
	
	public static Patient getPatientByLogin(String login) throws ClassNotFoundException, SQLException {
		return PatientDAO.getPatientByLogin(login);
	}
	
	public static Patient getPatientById(Integer id) throws ClassNotFoundException, SQLException {
		return PatientDAO.getPatientById(id);
	}
	
	public static void dischargePatientById(Integer patientId) throws ClassNotFoundException, SQLException {
		PatientDAO.dischargePatientById(patientId);
		Location location = LocationService.getLocationByPatientId(patientId);
		LocationService.releaseLocation(location.getWardNumber(), location.getBedNumber());
		PrescriptionService.deactivateTreatmentByPatientId(patientId);
	}
}

