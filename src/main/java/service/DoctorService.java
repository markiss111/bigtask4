package service;

import java.sql.SQLException;
import java.util.Collection;

import dao.DoctorDAO;
import models.Doctor;

public abstract class DoctorService {
	
	public static void addNewDoctor(String fname, String lname, String surname, String type, String phone, String address, String login, String password,
			String avatar) throws ClassNotFoundException, SQLException {
		DoctorDAO.addNewDoctor(fname, lname, surname, type, phone, address, login, password, avatar);
	}
	
	public static Collection<Doctor> getAllDoctors() throws ClassNotFoundException, SQLException {
		return DoctorDAO.getAllDoctors();
	}
	
	public static Doctor getDoctorById(Integer id) throws ClassNotFoundException, SQLException{
		return DoctorDAO.getDoctorById(id);
	}	
	
	public static Doctor getDoctorByLogin(String login) throws ClassNotFoundException, SQLException {
		return DoctorDAO.getDoctorByLogin(login);
	}
	
	public static void setDoctorFiredById(Integer doctorId) throws ClassNotFoundException, SQLException {
		DoctorDAO.setDoctorFiredById(doctorId);
	}
}

