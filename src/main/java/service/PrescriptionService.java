package service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import dao.PrescriptionDAO;
import models.Doctor;
import models.Patient;
import models.Prescription;

public abstract class PrescriptionService {
	
	public static Prescription getPrescriptionById(Integer prescriptionId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getPrescriptionById(prescriptionId);
	}
	
	public static void addPrescription(Patient patient, String type, Doctor doctor, String description, Integer frequency, Date startDate,
			Date finalDate, Date appointedDate) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.insertNewPrescription(patient, type, doctor, description, frequency, startDate, finalDate, appointedDate);
	}
	
	public static Collection<Prescription> getPatientPrescriptionsByDoctorId(Integer doctorId, Integer patientId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getPatientPrescriptionsByDoctorId(doctorId, patientId);
	}
	
	public static void deactivateOperationById(Integer prescriptionId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.deactivateOperationById(prescriptionId);
	}
	
	public static void deactivateTreatmentById(Integer prescriptionId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.deactivateTreatmentById(prescriptionId);
	}
	
	public static Collection<Prescription> getAllOperationsByDoctorId(Integer doctorId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getAllOperationsByDoctorId(doctorId);
	}
	
	public static Collection<Prescription> getActiveDoctorOperationsByCurrentDate(Integer doctorId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getActiveDoctorOperationsByCurrentDate(doctorId);
	}
	
	public static Collection<Prescription> getActivePatientPrescriptionsByCurrentDate(Integer patientId)
			throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getActivePatientPrescriptionsByCurrentDate(patientId);
	}
	
	public static Collection<Prescription> getActivePrescriptionsByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getActivePrescriptionsByPatientId(patientId);
	}
	
	public static Collection<Prescription> getCurrentDateActiveTreatmentsByNurseId(Integer nurseId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getCurrentDateActiveTreatmentsByNurseId(nurseId);
	}
	
	public static void setNurseByPrescriptionId(Integer nurseId, Integer prescriptionId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.setNurseByPrescriptionId(nurseId, prescriptionId);
	}
	
	public static void updateAppointedDateByPrescriptionId(Date newAppointedDate, Integer prescriptionId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.updateAppointedDateByPrescriptionId(newAppointedDate, prescriptionId);
	}
	
	public static void releasePrescriptionById(Integer prescriptionId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.releasePrescriptionById(prescriptionId);
	}
	
	public static Integer getNurseIdByTaskId(Integer taskId) throws ClassNotFoundException, SQLException {
		return PrescriptionDAO.getNurseIdByTaskId(taskId);
	}
	
	public static void deactivateTreatmentByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {
		PrescriptionDAO.deactivateTreatmentByPatientId(patientId);
	}
}

