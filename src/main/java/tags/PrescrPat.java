package tags;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Prescription;

public class PrescrPat extends SimpleTagSupport {
	private Prescription prescription;

	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}

	@Override
	public void doTag() throws JspException, IOException {
		
		if (prescription != null && prescription.getIsActive()) {
			JspWriter out = getJspContext().getOut();
			String type = prescription.getType();
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			
			out.println("<div class=\"item " + prescription.getType() + " col-sm-6 col-lg-4 col-xs-12 \">");
			out.println("<div class=\"team-member\">");
			out.println("<div class=\"member-details\">");
			out.println("<h6>" + prescription.getType() + "</h6>");
			out.println("<h4>" + prescription.getDescription() + "</h4>");
			out.println("<p>Frequency: " + (type.equals("treatment") ? prescription.getFrequency() + " times per day." : "—") + " </p>");
			out.println("<p>Start: " + (type.equals("treatment") ? dateFormat.format(prescription.getStartDate()) : "—") + "</p>");
			out.println("<p>End: " + (type.equals("treatment") ? dateFormat.format(prescription.getFinalDate()) : "—") + "</p>");
			out.println("<p>" + (type.equals("treatment") ? "Upcoming time: " + timeFormat.format(prescription.getAppointedDate())  : "Appointed date: " + dateTimeFormat.format(prescription.getAppointedDate())) + "</p>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
		}	
	}	
}