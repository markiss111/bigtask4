package tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.AbstractPersonal;
import models.Doctor;

public class Pers extends SimpleTagSupport {
	private AbstractPersonal personal;

	public void setPersonal(AbstractPersonal personal) {
		this.personal = personal;
	}

	@Override
	public void doTag() throws JspException, IOException {
		
		if (personal != null && personal.getIsPresent()) {
			JspWriter out = getJspContext().getOut();
			
			out.println("<div class=\"item " + ((personal instanceof Doctor) ? "doctor" : "nurse") + " col-md-6\">");
			out.println("<div class=\"team-member\">");
			out.println("<a data-toggle=\"modal\" data-target=\"#myModal" + ((personal instanceof Doctor) ? "doctor" : "nurse") + personal.getId() + "\">");
			out.println("<div class=\"team-img\">");
			out.println("<img src=\"" + personal.getAvatar() + "\" class=\"img-responsive\" alt=\"Doc\" />");
			out.println("</div>");
			out.println("</a>");
			out.println("<div class=\"member-details\">");
			out.println("<h6>" + ((personal instanceof Doctor) ? ((Doctor) personal).getType() : "Nurse") + "</h6>");
			out.println("<h4>" + personal.getFname() + " " + personal.getSurname() + "</h4>");
			out.println("<p>Phone: " + personal.getPhone() + "</p>");
			out.println("<p>Address: " + personal.getAddress() + "</p>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("<div class=\"modal fade\" id=\"myModal" + ((personal instanceof Doctor) ? "doctor" : "nurse") + personal.getId() + "\" role=\"dialog\" aria-hidden=\"true\">");
			out.println("<div class=\"modal-dialog\">");
			out.println("<div class=\"modal-content\">");
			out.println("<div class=\"modal-body\">");
			out.println("<div class=\"row\">");
			out.println("<div class=\"col-xs-12\">");
			out.println("<div class=\"col-md-6 no-padding col-xs-2\">");
			out.println(
					"<img src=\"" + personal.getAvatar() + "\" class=\"img-responsive\" alt=\"Patient\"/>");
			out.println("</div>");
			out.println("<div class=\"col-md-6 col-xs-10 team-pop-info\">");
			out.println("<button data-dismiss=\"modal\" class=\"m-close\"></button>");
			out.println("<h4>" + personal.getFname() + " " + personal.getSurname() + "</h4>");
			out.println("<ul class=\"tp-meta\">");
			out.println("<li><span>Specialization</span> <em>" + ((personal instanceof Doctor) ? ((Doctor) personal).getType() : "Nurse") + "</em></li>");
			out.println("<li><span>Phone Number</span> <em>" + personal.getPhone() + "</em></li>");
			out.println("<li><span>Address</span> <em>" + personal.getAddress() + "</em></li>");
			out.println("<form action=\"fire\" method=\"post\">");
			out.println("<input name=\"personalId\" type=\"hidden\" value=\"" + personal.getId() + "\">");
			out.println("<input name=\"personalType\" type=\"hidden\" value=\"" + ((personal instanceof Doctor) ? "doctor" : "nurse") + "\">");
			out.println("<div class=\"button-container\">");
			out.println("<button type=\"submit\" class=\"button green-butt\">");
			out.println("<span>Fire</span>");
			out.println("</button>");
			out.println("</div>");
			out.println("</form>");
			out.println("</ul>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
		}	
	}	
}