package tags;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Nurse;
import models.Prescription;
import service.LocationService;

public class Tasks extends SimpleTagSupport {
	private List<Prescription> tasks;
	private Nurse nurse;
	
	public void setTasks(List<Prescription> tasks) {
		this.tasks = tasks;
	}

	public void setNurse(Nurse nurse) {
		this.nurse = nurse;
	}



	@Override
	public void doTag() throws JspException, IOException {
		
		
			JspWriter out = getJspContext().getOut();
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			for (int i = 0; i < tasks.size(); i++){
				Prescription currentTask = tasks.get(i);
				boolean isPerforming = false;
				
				if (currentTask.getCurrentNurse() != null) {
					if (currentTask.getCurrentNurse().getId() == nurse.getId()) {
						isPerforming = true;
					} else {
						continue;
					}
				}				
								
				out.println("<div class=\"item " + (isPerforming ? "my-perf" : "my-active") + " col-sm-6 col-lg-4 col-xs-12 \">");
				out.println("<div class=\"team-member\">");
				out.println("<div class=\"member-details\">");
				out.println("<h6>" + currentTask.getType() + "</h6>");
				out.println("<h4>" + currentTask.getDescription() + "</h4>");
				out.println("<p>Frequency: " + currentTask.getFrequency() + " times per day.</p>");
				out.println("<p>Patient: " + currentTask.getPatient().getFname() + " " + currentTask.getPatient().getSurname() + "</p>");
				try {
					out.println("<p>Ward: " + LocationService.getLocationByPatientId(currentTask.getPatient().getId()).getWardNumber() + "</p>");
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				out.println("<p>Appointed time: " + timeFormat.format(currentTask.getAppointedDate()) + "</p>");
				out.println("<form action=\"" + (isPerforming ? "confirm" : "perform") + "_task\" method=\"post\">");
				out.println("<input name=\"taskId\" type=\"hidden\" value=\"" + currentTask.getId() + "\">");
				out.println("<div class=\"button-container\">");
				out.println("<button type=\"submit\" class=\"button " + (isPerforming ? "green2-butt" : "") + "\">");
				out.println("<span>" + (isPerforming ? "Done" : "Perform") + "</span>");
				out.println("</button>");
				out.println("</div>");
				out.println("</form>");
				out.println("</div>");
				out.println("</div>");
				out.println("</div>");
				
			}
	
	}	
}