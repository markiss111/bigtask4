package tags;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Prescription;

public class UpPrescr extends SimpleTagSupport {
	private List<Prescription> prescriptions;

	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public void doTag() throws JspException, IOException {

		JspWriter out = getJspContext().getOut();
		for (int i = 0; i < 5; i++){
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			if (prescriptions == null || i >= prescriptions.size() || !prescriptions.get(i).getIsActive()) {
				out.println("<li>—</li>");
			} else {
				out.println("<li>" + prescriptions.get(i).getDescription() + " <span class=\"pull-right\">" + timeFormat.format(prescriptions.get(i).getAppointedDate()) + "</span></li>");
			}
		}
		
	}

}
