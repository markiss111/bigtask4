package tags;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Location;
import models.Patient;
import models.Prescription;
import service.LocationService;

public class UpOper extends SimpleTagSupport {
	private List<Prescription> operations;

	public void setOperations(List<Prescription> operations) {
		this.operations = operations;
	}

	public void doTag() throws JspException, IOException {

		JspWriter out = getJspContext().getOut();
		for (int i = 0; i < 5; i++){
			
			if (operations == null || operations.size() == 0) {
				out.println("<div class=\"item sidebox-timing\">");
				out.println("<h2>No Operations</h2>");
				out.println("<h6>Operation</h6>");
				out.println("<p>Upcoming operation</p>");
				out.println("<ul class=\"list-unstyled timings\">");
				out.println("<li>Patient <span class=\"pull-right\">—</span></li>");
				out.println("<li>Diagnosis <span class=\"pull-right\">—</span></li>");
				out.println("<li>Ward <span class=\"pull-right\">—</span></li>");
				out.println("<li>Time <span class=\"pull-right\">—</span></li>");
				break;
			}
			
			if (i == operations.size()) {
				break;
			}
			
			Patient patient = operations.get(i).getPatient();
			Prescription currentTask = operations.get(i);
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			Location location = null;
			try {
				location = LocationService.getLocationByPatientId(patient.getId());
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			out.println("<div class=\"item sidebox-timing\">");
			out.println("<h2>" + currentTask.getDescription() + "</h2>");
			out.println("<h6>Operation</h6>");
			out.println("<p>Upcoming operation</p>");
			out.println("<ul class=\"list-unstyled timings\">");
			out.println("<li>Patient <span class=\"pull-right\"<strong>" + patient.getFname() + " " + patient.getSurname() + "</strong></span></li>");
			out.println("<li>Diagnosis <span class=\"pull-right\">" + patient.getDiagnosis() + "</span></li>");
			out.println("<li>Ward <span class=\"pull-right\">" + (location == null ? "—" : location.getWardNumber()) + "</span></li>");
			out.println("<li>Time <span class=\"pull-right\">" + timeFormat.format(currentTask.getAppointedDate()) + "</span></li>");
			out.println("</ul>");
			out.println("</div>");
			
		}
		
	}

}
