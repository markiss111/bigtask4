package tags;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Location;
import models.Nurse;
import models.Patient;
import models.Prescription;
import service.LocationService;

public class UpTasks extends SimpleTagSupport {
	private List<Prescription> tasks;
	private Nurse nurse;

	public void setTasks(List<Prescription> tasks) {
		this.tasks = tasks;
	}

	public void setNurse(Nurse nurse) {
		this.nurse = nurse;
	}



	public void doTag() throws JspException, IOException {

		JspWriter out = getJspContext().getOut();
		for (int i = 0; i < 5; i++){
	
			if (tasks == null || tasks.size() == 0) {
				out.println("<div class=\"item sidebox-timing\">");
				out.println("<h2>No Tasks</h2>");
				out.println("<h6>Prescription</h6>");
				out.println("<p>Upcoming task</p>");
				out.println("<ul class=\"list-unstyled timings\">");
				out.println("<li>Patient <span class=\"pull-right\">—</span></li>");	
				out.println("<li>Ward <span class=\"pull-right\">—</span></li>");
				out.println("<li>Time <span class=\"pull-right\">—</span></li>");
				break;
			}
			
			if (i == tasks.size()) {
				break;
			}
			
			Prescription currentTask = tasks.get(i);
			boolean isPerforming = false;
			
			if (currentTask.getCurrentNurse() != null) {
				if (currentTask.getCurrentNurse().getId() == nurse.getId()) {
					isPerforming = true;
				} else {
					continue;
				}
			}	
			
			Patient patient = tasks.get(i).getPatient();
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
			Location location = null;
			try {
				location = LocationService.getLocationByPatientId(patient.getId());
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			out.println("<div class=\"item sidebox-timing\">");
			out.println("<h2>" + currentTask.getDescription() + "</h2>");
			out.println("<h6>" + (isPerforming ? "Performing Task" : "New Task") + "</h6>");
			out.println("<p>Upcoming task</p>");
			out.println("<ul class=\"list-unstyled timings\">");
			out.println("<li>Patient <span class=\"pull-right\"<strong>" + patient.getFname() + " " + patient.getSurname() + "</strong></span></li>");
			out.println("<li>Ward <span class=\"pull-right\">" + (location == null ? "—" : location.getWardNumber()) + "</span></li>");
			out.println("<li>Time <span class=\"pull-right\">" + timeFormat.format(currentTask.getAppointedDate()) + "</span></li>");
			
			out.println("<form action=\"" + (isPerforming ? "confirm" : "perform") + "_task\" method=\"post\">");
			out.println("<input name=\"taskId\" type=\"hidden\" value=\"" + currentTask.getId() + "\">");
			out.println("<div class=\"button-container\">");
			out.println("<button type=\"submit\" class=\"button " + (isPerforming ? "green2-butt" : "") + "\">");
			out.println("<span style=\"color: #fff;\">" + (isPerforming ? "Done" : "Perform") + "</span>");
			out.println("</button>");
			out.println("</div>");
			out.println("</form>");
			
			out.println("</ul>");
			out.println("</div>");		
		}
		
	}

}
