package tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Location;

public class Locs extends SimpleTagSupport {
	private List<Location> locations;

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		int temp = 1;
		for (int i = 0; i < locations.size(); ){

			out.println("<div class=\"col-xs-5 no-padding\">");
			out.println("<div class=\"row\">");
			out.println("<div class=\"col-xs-12 text-center\">");
			out.println("<h3>Ward #" + locations.get(i).getWardNumber() + "</h3>");
			out.println("</div>");

			while (i < locations.size() && locations.get(i).getWardNumber() == temp){
				out.println("<div class=\"col-xs-4\"><i id=\"" + locations.get(i).getWardNumber() + locations.get(i).getBedNumber() + "\" class=\"fa fa-bed fa-2x bed " + (locations.get(i).getPatient() == null ? "free" : "occupied") + "\" aria-hidden=\"true\"></i></div>");
				i++;
			}
			
			out.println("</div>");
			out.println("<hr>");
			out.println("</div>");
			
			if (temp%2 != 0) {
				out.println("<div class=\"col-xs-2\"></div>");
			}
			
			temp++;
		}
		out.println("<div class=\"row\">");
		out.println("<div class=\"col-xs-12 text-center\">");
		out.println("<br><br>");
		out.println("</div>");
		out.println("</div>");
	}

}
