package tags;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import models.Patient;
import service.LocationService;

public class Pat extends SimpleTagSupport {
	private Patient patient;

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void doTag() throws JspException, IOException {

		if (patient != null && patient.getDischargeDate() == null) {
			JspWriter out = getJspContext().getOut();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			
			out.println("<div class=\"item col-md-6\">");
			out.println("<div class=\"team-member\">");
			out.println("<a data-toggle=\"modal\" data-target=\"#myModal" + patient.getId() + "\">");
			out.println("<div class=\"team-img\">");
			out.println("<img src=\"" + patient.getAvatar() + "\" class=\"img-responsive\" alt=\"Doc\" />");
			out.println("</div>");
			out.println("</a>");
			out.println("<div class=\"member-details\">");
			out.println("<h6>" + patient.getDiagnosis() + "</h6>");
			out.println("<h4>" + patient.getFname() + " " + patient.getSurname() + "</h4>");
			try {
				out.println("<p>Ward: " + LocationService.getLocationByPatientId(patient.getId()).getWardNumber() + "</p>");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.println("<p>Enter date: " + dateFormat.format(patient.getEnterDate()) + "</p>");
			out.println("<p>Phone: " + patient.getPhone() + "</p>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("<div class=\"modal fade\" id=\"myModal" + patient.getId() + "\" role=\"dialog\" aria-hidden=\"true\">");
			out.println("<div class=\"modal-dialog\">");
			out.println("<div class=\"modal-content\">");
			out.println("<div class=\"modal-body\">");
			out.println("<div class=\"row\">");
			out.println("<div class=\"col-xs-12\">");
			out.println("<div class=\"col-md-6 no-padding col-xs-2\">");
			out.println(
					"<img src=\"" + patient.getAvatar() + "\" class=\"img-responsive\" alt=\"Patient\"/>");
			out.println("</div>");
			out.println("<div class=\"col-md-6 col-xs-10 team-pop-info\">");
			out.println("<button data-dismiss=\"modal\" class=\"m-close\"></button>");
			out.println("<h4>" + patient.getFname() + " " + patient.getSurname() + "</h4>");
			out.println("<ul class=\"tp-meta\">");
			out.println("<li><span>Diagnosis</span> <em>" + patient.getDiagnosis() + "</em></li>");
			try {
				out.println("<li><span>Ward</span> <em>" + LocationService.getLocationByPatientId(patient.getId()).getWardNumber() + "</em></li>");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.println("<li><span>Enter Date</span> <em>" + dateFormat.format(patient.getEnterDate()) + "</em></li>");
			out.println("<li><span>Phone Number</span> <em>" + patient.getPhone() + "</em></li>");
			out.println("<li><span>Address</span> <em>" + patient.getAddress() + "</em></li>");
			out.println("<li>");
			out.println("<span>Prescriptions</span>");
			out.println("<form action=\"view_patient_prescriptions\" method=\"post\">");
			out.println("<input name=\"patientId\" type=\"hidden\" value=\"" + patient.getId() + "\">");
			out.println("<input name=\"patientFullName\" type=\"hidden\" value=\"" + patient.getFname() + " " + patient.getSurname() + "\">");
			out.println("<div class=\"button-container small-cont\">");
			out.println("<button type=\"submit\" class=\"button small-butt\">");
			out.println("<span>View</span>");
			out.println("</button>");
			out.println("</div>");
			out.println("</form>");
			out.println("<form action=\"create_prescription\" method=\"post\">");
			out.println("<input name=\"patientId\" type=\"hidden\" value=\"" + patient.getId() + "\">");
			out.println("<input name=\"patientFullName\" type=\"hidden\" value=\"" + patient.getFname() + " " + patient.getSurname() + "\">");
			out.println("<div class=\"button-container small-cont\">");
			out.println("<button type=\"submit\" class=\"button small-butt\">");
			out.println("<span>Add</span>");
			out.println("</button>");
			out.println("</div>");
			out.println("</form>");
			out.println("</li>");
			out.println("<form action=\"discharge\" method=\"post\">");
			out.println("<input name=\"patientId\" type=\"hidden\" value=\"" + patient.getId() + "\">");
			out.println("<div class=\"button-container\">");
			out.println("<button type=\"submit\" class=\"button green-butt\">");
			out.println("<span>Discharge</span>");
			out.println("</button>");
			out.println("</div>");
			out.println("</form>");
			out.println("</ul>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
			out.println("</div>");
		}
	}

}
