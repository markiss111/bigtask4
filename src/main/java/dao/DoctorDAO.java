package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.Doctor;
import persistant.ConnectionManager;
import transformers.DoctorTransformer;

public abstract class DoctorDAO {
	private static final String SQL_INSERT_NEW_DOCTOR = "INSERT INTO doctors VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_GET_ALL_DOCTORS = "SELECT * FROM doctors";
	private static final String SQL_GET_DOCTOR_BY_ID = "SELECT * FROM doctors WHERE doctor_id = ?";
	private static final String SQL_GET_DOCTOR_BY_LOGIN = "SELECT * FROM doctors WHERE login = ?";
	private static final String SQL_SET_DOCTOR_IS_NOT_PRESENT_BY_ID = "UPDATE doctors SET is_present = ? WHERE doctor_id = ?";
	
	public static void addNewDoctor(String fname, String lname, String surname, String type, String phone, String address, String login, String password,
			String avatar) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_INSERT_NEW_DOCTOR);
		
		pStatement.setString(1, null);
		pStatement.setString(2, fname);
		pStatement.setString(3, lname);
		pStatement.setString(4, surname);
		pStatement.setString(5, type);
		pStatement.setString(6, phone);
		pStatement.setString(7, address);
		pStatement.setString(8, login);
		pStatement.setString(9, password);
		pStatement.setString(10, avatar);
		pStatement.setBoolean(11, true);
		
		pStatement.executeUpdate();
	}
	
	public static Collection<Doctor> getAllDoctors() throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_ALL_DOCTORS);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return DoctorTransformer.fromRSToCollection(resultSet);
	}
	
	public static Doctor getDoctorById(Integer id) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_DOCTOR_BY_ID);

		pStatement.setInt(1, id);
		ResultSet resultSet = pStatement.executeQuery();
		
		return DoctorTransformer.fromRSToDoctor(resultSet);
	}
	
	public static Doctor getDoctorByLogin(String login) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_DOCTOR_BY_LOGIN);

		pStatement.setString(1, login);
		ResultSet resultSet = pStatement.executeQuery();
		
		return DoctorTransformer.fromRSToDoctor(resultSet);
	}
	
	public static void setDoctorFiredById(Integer doctorId) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_SET_DOCTOR_IS_NOT_PRESENT_BY_ID);
		
		pStatement.setBoolean(1, false);
		pStatement.setInt(2, doctorId);
		
		pStatement.executeUpdate();
	}
}