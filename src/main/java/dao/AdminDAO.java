package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.Admin;
import persistant.ConnectionManager;
import transformers.AdminTransformer;

public abstract class AdminDAO {
	private static final String SQL_GET_ADMIN_BY_LOGIN = "SELECT * FROM admins WHERE login = ?";
	
	
	public static Admin getAdminByLogin(String login) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_ADMIN_BY_LOGIN);

		pStatement.setString(1, login);
		ResultSet resultSet = pStatement.executeQuery();
		
		return AdminTransformer.fromRSToAdmin(resultSet);
	}
	
	
}