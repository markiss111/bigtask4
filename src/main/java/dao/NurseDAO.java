package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.Nurse;
import persistant.ConnectionManager;
import transformers.NurseTransformer;

public abstract class NurseDAO {
	private static final String SQL_INSERT_NEW_NURSE = "INSERT INTO nurses VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_GET_ALL_NURSES = "SELECT * FROM nurses";
	private static final String SQL_GET_NURSE_BY_LOGIN = "SELECT * FROM nurses WHERE login = ?";
	private static final String SQL_GET_NURSE_BY_ID = "SELECT * FROM nurses WHERE nurse_id = ?";
	private static final String SQL_SET_NURSE_IS_NOT_PRESENT_BY_ID = "UPDATE nurses SET is_present = ? WHERE nurse_id = ?";
	
	public static void addNewNurse(String fname, String lname, String surname, String phone, String address, String login, String password,
			String avatar) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_INSERT_NEW_NURSE);
		
		pStatement.setString(1, null);
		pStatement.setString(2, fname);
		pStatement.setString(3, lname);
		pStatement.setString(4, surname);
		pStatement.setString(5, phone);
		pStatement.setString(6, address);
		pStatement.setString(7, login);
		pStatement.setString(8, password);
		pStatement.setString(9, avatar);
		pStatement.setBoolean(10, true);
		
		pStatement.executeUpdate();
	}
	
	public static Collection<Nurse> getAllNurses() throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_ALL_NURSES);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return NurseTransformer.fromRSToCollection(resultSet);
	}
	
	public static Nurse getNurseByLogin(String login) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_NURSE_BY_LOGIN);

		pStatement.setString(1, login);
		ResultSet resultSet = pStatement.executeQuery();
		
		return NurseTransformer.fromRSToNurse(resultSet);
	}
	
	public static Nurse getNurseById(Integer nurseId) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_NURSE_BY_ID);
		
		pStatement.setInt(1, nurseId);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return NurseTransformer.fromRSToNurse(resultSet);
	}
	
	public static void setNurseFiredById(Integer nurseId) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_SET_NURSE_IS_NOT_PRESENT_BY_ID);
		
		pStatement.setBoolean(1, false);
		pStatement.setInt(2, nurseId);
		
		pStatement.executeUpdate();
	}
	
	
}