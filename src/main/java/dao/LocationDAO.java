package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.Location;
import persistant.ConnectionManager;
import transformers.LocationTransfromer;

public abstract class LocationDAO {
	private static final String SQL_GET_ALL_LOCATIONS_ORDERED_BY_NUMBERS = "SELECT * FROM locations ORDER BY ward_number AND bed_number";
	private static final String SQL_SET_PATIENT_BY_NUMBERS = "UPDATE locations SET patient_id = ? WHERE ward_number = ? AND bed_number = ?";
	private static final String SQL_GET_LOCATION_BY_PATIENT_ID = "SELECT * FROM locations WHERE patient_id = ?"; 
	private static final String SQL_RELEASE_LOCATION_BY_NUMBERS = "UPDATE locations SET patient_id = ? WHERE ward_number = ? AND bed_number = ?";

	public static Collection<Location> getAllLocations() throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_GET_ALL_LOCATIONS_ORDERED_BY_NUMBERS);

		ResultSet resultSet = pStatement.executeQuery();

		return LocationTransfromer.fromRSToCollection(resultSet);
	}

	public static void setPatientByNumbers(Integer patientId, Integer wardNumber, Integer bedNumber)
			throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_SET_PATIENT_BY_NUMBERS);

		pStatement.setInt(1, patientId);
		pStatement.setInt(2, wardNumber);
		pStatement.setInt(3, bedNumber);

		pStatement.executeUpdate();
	}
	
	public static Location getLocationByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_LOCATION_BY_PATIENT_ID);

		pStatement.setInt(1, patientId);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return LocationTransfromer.fromRSToLocation(resultSet);
	}

	public static void releaseLocation(Integer wardNumber, Integer bedNumber) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_RELEASE_LOCATION_BY_NUMBERS);

		pStatement.setString(1, null);
		pStatement.setInt(2, wardNumber);
		pStatement.setInt(3, bedNumber);

		pStatement.executeUpdate();
	}
}
