package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import models.Doctor;
import models.Patient;
import persistant.ConnectionManager;
import transformers.PatientTransformer;

public abstract class PatientDAO {
	private static final String SQL_INSERT_NEW_PATIENT = "INSERT INTO patients VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_GET_PATIENTS_BY_DOCTOR_ID = "SELECT * FROM patients WHERE doctor_id = ? ORDER BY enter_date DESC";
	private static final String SQL_GET_PATIENT_BY_LOGIN = "SELECT * FROM patients WHERE login = ?";
	private static final String SQL_GET_PATIENT_BY_ID = "SELECT * FROM patients WHERE patient_id = ?";
	private static final String SQL_SET_DISCHARGE_DATE_BY_ID = "UPDATE patients SET discharge_date = ? WHERE patient_id = ?";
	
	public static void addNewPatient(String fname, String lname, String surname, String phone, String address, Doctor doctor,
			String diagnosis, String login, String password,
			String avatar) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_INSERT_NEW_PATIENT);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(new Date());
		
		pStatement.setString(1, null);
		pStatement.setString(2, fname);
		pStatement.setString(3, lname);
		pStatement.setString(4, surname);
		pStatement.setString(5, phone);
		pStatement.setString(6, address);
		pStatement.setInt(7, doctor.getId());
		pStatement.setString(8, diagnosis);
		pStatement.setString(9, currentTime);
		pStatement.setString(10, null);
		pStatement.setString(11, login);
		pStatement.setString(12, password);
		pStatement.setString(13, avatar);
		
		pStatement.executeUpdate();
	}
	
	public static Collection<Patient> getPatientsByDoctorId(Integer id) throws SQLException, ClassNotFoundException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_PATIENTS_BY_DOCTOR_ID);
		pStatement.setInt(1, id);
		ResultSet resultSet = pStatement.executeQuery();
		return PatientTransformer.fromRSToCollection(resultSet);	
	}
	
	public static Patient getPatientByLogin(String login) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_PATIENT_BY_LOGIN);
		
		pStatement.setString(1, login);
		ResultSet resultSet = pStatement.executeQuery();
		
		return PatientTransformer.fromRSToPatient(resultSet);	
	}
	
	public static Patient getPatientById(Integer id) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_PATIENT_BY_ID);
		
		pStatement.setInt(1, id);
		ResultSet resultSet = pStatement.executeQuery();
		
		return PatientTransformer.fromRSToPatient(resultSet);	
	}
	
	public static void dischargePatientById(Integer id) throws ClassNotFoundException, SQLException {
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_SET_DISCHARGE_DATE_BY_ID);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dischargeTime = sdf.format(new Date());
		
		pStatement.setString(1, dischargeTime);
		pStatement.setInt(2, id);
		
		pStatement.executeUpdate();
	}
}