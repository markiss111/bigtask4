package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import models.Doctor;
import models.Patient;
import models.Prescription;
import persistant.ConnectionManager;
import transformers.PrescriptionTransformer;

public abstract class PrescriptionDAO {
	private static final String SQL_GET_PRESCRIPTION_BY_ID = "SELECT * FROM prescriptions WHERE prescription_id = ?";
	private static final String SQL_INSERT_NEW_PRESCRIPTION = "INSERT INTO prescriptions VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_GET_PATIENT_PRESCRIPTIONS_BY_DOCTOR_ID = "SELECT * FROM prescriptions WHERE doctor_id = ? AND patient_id = ?";
	private static final String SQL_DEACTIVATE_OPERATION_BY_ID = "UPDATE prescriptions SET is_active = ? WHERE prescription_id = ?";
	private static final String SQL_DEACTIVATE_TREATMENT_BY_ID = "UPDATE prescriptions SET is_active = ?, final_date = ?, nurse_id = ? WHERE prescription_id = ?";
	private static final String SQL_DEACTIVATE_TREATMENT_BY_PATIENT_ID = "UPDATE prescriptions SET is_active = ?, final_date = ?, nurse_id = ? WHERE patient_id = ?";
	private static final String SQL_GET_OPERATIONS_BY_DOCTOR_ID = "SELECT * FROM prescriptions WHERE doctor_id = ? AND type = ? ORDER BY appointed_date";
	private static final String SQL_UPDATE_ALL_OPERATIONS_BY_CURRENT_DATE = "UPDATE prescriptions SET is_active = FALSE WHERE type = ? AND appointed_date < ?";
	private static final String SQL_UPDATE_ALL_TREATMENTS_BY_CURRENT_DATE = "UPDATE prescriptions SET is_active = FALSE, nurse_id = ? WHERE type = ? AND final_date < ?";
	private static final String SQL_GET_ACTIVE_OPERATIONS_BY_DOCTOR_ID_BY_CURRENT_DATE = "SELECT * FROM prescriptions WHERE type = ? AND doctor_id = ? AND is_active = ? AND DATE(appointed_date) = ? ORDER BY appointed_date";
	private static final String SQL_GET_ACTIVE_PRESCRIPTIONS_BY_PATIENT_ID_BY_CURRENT_DATE = "SELECT * FROM prescriptions WHERE patient_id = ? AND is_active = ? AND DATE(appointed_date) = ? ORDER BY appointed_date";
	private static final String SQL_GET_ACTIVE_PRESCRIPTIONS_BY_PATIENT_ID = "SELECT * FROM prescriptions WHERE patient_id = ? AND is_active = ? ORDER BY appointed_date";
	private static final String SQL_GET_CURRENT_DATE_ACTIVE_TREATMENTS_BY_NURSE_ID = "SELECT * FROM prescriptions WHERE type = ? AND is_active = ? AND DATE(appointed_date) <= ? AND appointed_date <= ? AND (nurse_id IS ? OR nurse_id = ?) ORDER BY appointed_date";
	private static final String SQL_SET_NURSE_BY_PRESCRIPTION_ID = "UPDATE prescriptions SET nurse_id = ? WHERE prescription_id = ?";
	private static final String SQL_UPDATE_APPOINTED_DATE_BY_PRESCRIPTION_ID = "UPDATE prescriptions SET appointed_date = ? WHERE prescription_id = ?";
	private static final String SQL_RELEASE_PRESCRIPTION_BY_ID = "UPDATE prescriptions SET nurse_id = NULL WHERE prescription_id = ?";
	private static final String SQL_GET_NURSE_ID_BY_TASK_ID = "SELECT nurse_id FROM prescriptions WHERE prescription_id IS NOT NULL AND prescription_id = ?";
	
	public static Prescription getPrescriptionById(Integer prescriptionId) throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_PRESCRIPTION_BY_ID);
		pStatement.setInt(1, prescriptionId);
		ResultSet resultSet = pStatement.executeQuery();

		return PrescriptionTransformer.fromRSToPrescription(resultSet);
	}

	public static void insertNewPrescription(Patient patient, String type, Doctor doctor, String description,
			Integer frequency, Date startDate, Date finalDate, Date appointedDate)
			throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_INSERT_NEW_PRESCRIPTION);

		SimpleDateFormat finalDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat appDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String fDateTime = finalDate == null ? null : finalDateFormat.format(finalDate);
		String appDateTime = appDateFormat.format(appointedDate);
		String currentTime = startDate == null ? null : startDateFormat.format(startDate);

		pStatement.setString(1, null);
		pStatement.setInt(2, patient.getId());
		pStatement.setString(3, type);
		pStatement.setInt(4, doctor.getId());
		pStatement.setString(5, description);
		pStatement.setInt(6, frequency);
		pStatement.setString(7, currentTime);
		pStatement.setString(8, fDateTime);
		pStatement.setString(9, appDateTime);
		pStatement.setBoolean(10, true);
		pStatement.setString(11, null);

		pStatement.executeUpdate();
	}

	public static Collection<Prescription> getPatientPrescriptionsByDoctorId(Integer doctorId, Integer patientId)
			throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_GET_PATIENT_PRESCRIPTIONS_BY_DOCTOR_ID);

		pStatement.setInt(1, doctorId);
		pStatement.setInt(2, patientId);

		ResultSet resultSet = pStatement.executeQuery();

		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}

	public static void deactivateOperationById(Integer prescriptionId) throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_DEACTIVATE_OPERATION_BY_ID);

		pStatement.setBoolean(1, false);
		pStatement.setInt(2, prescriptionId);

		pStatement.executeUpdate();
	}

	public static void deactivateTreatmentById(Integer prescriptionId) throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_DEACTIVATE_TREATMENT_BY_ID);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentTime = dateFormat.format(new Date());

		pStatement.setBoolean(1, false);
		pStatement.setString(2, currentTime);
		pStatement.setString(3, null);
		pStatement.setInt(4, prescriptionId);
		
		pStatement.executeUpdate();
	}
	
	public static void deactivateTreatmentByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_DEACTIVATE_TREATMENT_BY_PATIENT_ID);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentTime = dateFormat.format(new Date());

		pStatement.setBoolean(1, false);
		pStatement.setString(2, currentTime);
		pStatement.setString(3, null);
		pStatement.setInt(4, patientId);
		
		pStatement.executeUpdate();
	}
	
	

	public static Collection<Prescription> getAllOperationsByDoctorId(Integer doctorId)
			throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_GET_OPERATIONS_BY_DOCTOR_ID);

		pStatement.setInt(1, doctorId);
		pStatement.setString(2, "operation");

		ResultSet resultSet = pStatement.executeQuery();

		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}

	public static Collection<Prescription> getActiveDoctorOperationsByCurrentDate(Integer doctorId)
			throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_GET_ACTIVE_OPERATIONS_BY_DOCTOR_ID_BY_CURRENT_DATE);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = dateFormat.format(new Date());

		pStatement.setString(1, "operation");
		pStatement.setInt(2, doctorId);
		pStatement.setBoolean(3, true);
		pStatement.setString(4, currentDate);
		ResultSet resultSet = pStatement.executeQuery();

		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}
	
	public static Collection<Prescription> getActivePatientPrescriptionsByCurrentDate(Integer patientId)
			throws ClassNotFoundException, SQLException {

		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_GET_ACTIVE_PRESCRIPTIONS_BY_PATIENT_ID_BY_CURRENT_DATE);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = dateFormat.format(new Date());

		pStatement.setInt(1, patientId);
		pStatement.setBoolean(2, true);
		pStatement.setString(3, currentDate);
		ResultSet resultSet = pStatement.executeQuery();

		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}

	public static Collection<Prescription> getActivePrescriptionsByPatientId(Integer patientId) throws ClassNotFoundException, SQLException {
		
		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_ACTIVE_PRESCRIPTIONS_BY_PATIENT_ID);
		
		pStatement.setInt(1, patientId);
		pStatement.setBoolean(2, true);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}
	
	public static Collection<Prescription> getCurrentDateActiveTreatmentsByNurseId(Integer nurseId) throws ClassNotFoundException, SQLException {
		
		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection().prepareStatement(SQL_GET_CURRENT_DATE_ACTIVE_TREATMENTS_BY_NURSE_ID);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String currentDate = dateFormat.format(new Date());
		String currentDateTime = dateTimeFormat.format(new Date());
		
		pStatement.setString(1, "treatment");
		pStatement.setBoolean(2, true);
		pStatement.setString(3, currentDate);
		pStatement.setString(4, currentDateTime);
		pStatement.setString(5, null);
		pStatement.setInt(6, nurseId);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		return PrescriptionTransformer.fromRSToCollection(resultSet);
	}
	
	public static void setNurseByPrescriptionId(Integer nurseId, Integer prescriptionId) throws ClassNotFoundException, SQLException {
		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_SET_NURSE_BY_PRESCRIPTION_ID);
		
		pStatement.setInt(1, nurseId);
		pStatement.setInt(2, prescriptionId);
		
		pStatement.executeUpdate();
	}
	
	public static void releasePrescriptionById(Integer prescriptionId) throws ClassNotFoundException, SQLException {
		
		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
		.prepareStatement(SQL_RELEASE_PRESCRIPTION_BY_ID);
		
		pStatement.setInt(1, prescriptionId);
		
		pStatement.executeUpdate();
	}
	
	public static void updateAppointedDateByPrescriptionId(Date newAppointedDate, Integer prescriptionId) throws ClassNotFoundException, SQLException {
		updateAllPrescriptionsByCurrentDate();
		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_UPDATE_APPOINTED_DATE_BY_PRESCRIPTION_ID);
		
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		pStatement.setString(1, dateTimeFormat.format(newAppointedDate));
		pStatement.setInt(2, prescriptionId);
		
		pStatement.executeUpdate();
	}
	
	public static Integer getNurseIdByTaskId(Integer taskId) throws ClassNotFoundException, SQLException {
		
		PreparedStatement pStatement = ConnectionManager.getConnection()
		.prepareStatement(SQL_GET_NURSE_ID_BY_TASK_ID);
		
		pStatement.setInt(1, taskId);
		
		ResultSet resultSet = pStatement.executeQuery();
		
		resultSet.next();
		
		return resultSet.getInt("nurse_id");
		
	}
	
	public static void updateAllOperaionsByCurrentDate() throws ClassNotFoundException, SQLException {

		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_UPDATE_ALL_OPERATIONS_BY_CURRENT_DATE);

		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = dateTimeFormat.format(new Date());

		pStatement.setString(1, "operation");
		pStatement.setString(2, currentTime);

		pStatement.executeUpdate();
	}
	
	public static void updateAllTreatmentsByCurrentDate() throws ClassNotFoundException, SQLException {

		PreparedStatement pStatement = ConnectionManager.getConnection()
				.prepareStatement(SQL_UPDATE_ALL_TREATMENTS_BY_CURRENT_DATE);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = dateFormat.format(new Date());

		pStatement.setString(1, null);
		pStatement.setString(2, "treatment");
		pStatement.setString(3, currentDate);

		pStatement.executeUpdate();
	}

	public static void updateAllPrescriptionsByCurrentDate() throws ClassNotFoundException, SQLException {
		updateAllOperaionsByCurrentDate();
		updateAllTreatmentsByCurrentDate();
	}

}