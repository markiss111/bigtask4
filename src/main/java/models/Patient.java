package models;

import java.util.Date;

public class Patient {
	private Integer id;
	private String fname;
	private String lname;
	private String surname;
	private String phone;
	private String address;
	private Doctor doctor;
	private String diagnosis;
	private Date enterDate;
	private Date dischargeDate;
	private String login;
	private String password;
	private String avatar;
	public Patient() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Patient(Integer id, String fname, String lname, String surname, String phone, String address, Doctor doctor,
			String diagnosis, Date enterDate, Date dischargeDate, String login, String password,
			String avatar) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.surname = surname;
		this.phone = phone;
		this.address = address;
		this.doctor = doctor;
		this.diagnosis = diagnosis;
		this.enterDate = enterDate;
		this.dischargeDate = dischargeDate;
		this.login = login;
		this.password = password;
		this.avatar = avatar;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public Date getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}
	public Date getDischargeDate() {
		return dischargeDate;
	}
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}
