package models;

public class Doctor extends AbstractPersonal {
	private String type;

	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Doctor(Integer id, String fname, String lname, String surname, String type, String phone, String address, String login,
			String password, String avatar, Boolean isPresent) {
		super(id, fname, lname, surname, phone, address, login, password, avatar, isPresent);
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
