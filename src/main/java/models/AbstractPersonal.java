package models;

public abstract class AbstractPersonal {
	private Integer id;
	private String fname;
	private String lname;
	private String surname;
	private String phone;
	private String address;
	private String login;
	private String password;
	private String avatar;
	private Boolean isPresent;
	public AbstractPersonal(Integer id, String fname, String lname, String surname, String phone, String address,
			String login, String password, String avatar, Boolean isPresent) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.surname = surname;
		this.phone = phone;
		this.address = address;
		this.login = login;
		this.password = password;
		this.avatar = avatar;
		this.isPresent = isPresent;
	}
	public AbstractPersonal() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Boolean getIsPresent() {
		return isPresent;
	}
	public void setIsPresent(Boolean isPresent) {
		this.isPresent = isPresent;
	}
}
