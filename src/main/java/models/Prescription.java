package models;

import java.util.Date;

public class Prescription {
	private Integer id;
	private Patient patient;
	private String type;
	private Doctor doctor;
	private String description;
	private Integer frequency;
	private Date startDate;
	private Date finalDate;
	private Date appointedDate;
	private Boolean isActive;

	// Only for a nurses
	// Nurse, who is currently performing a treatment
	private Nurse currentNurse;

	public Prescription(Integer id, Patient patient, String type, Doctor doctor, String description, Integer frequency,
			Date startDate, Date finalDate, Date appointedDate, Boolean isActive, Nurse currentNurse) {
		super();
		this.id = id;
		this.patient = patient;
		this.type = type;
		this.doctor = doctor;
		this.description = description;
		this.frequency = frequency;
		this.startDate = startDate;
		this.finalDate = finalDate;
		this.appointedDate = appointedDate;
		this.isActive = isActive;
		this.currentNurse = currentNurse;
	}

	public Prescription() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Date getAppointedDate() {
		return appointedDate;
	}

	public void setAppointedDate(Date appointedDate) {
		this.appointedDate = appointedDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Nurse getCurrentNurse() {
		return currentNurse;
	}

	public void setCurrentNurse(Nurse currentNurse) {
		this.currentNurse = currentNurse;
	}
}
