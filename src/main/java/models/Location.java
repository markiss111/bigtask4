package models;

public class Location {
	private Integer wardNumber;
	private Integer bedNumber;
	private Patient patient;
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Location(Integer wardNumber, Integer bedNumber, Patient patient) {
		super();
		this.wardNumber = wardNumber;
		this.bedNumber = bedNumber;
		this.patient = patient;
	}
	public Integer getWardNumber() {
		return wardNumber;
	}
	public void setWardNumber(Integer wardNumber) {
		this.wardNumber = wardNumber;
	}
	public Integer getBedNumber() {
		return bedNumber;
	}
	public void setBedNumber(Integer bedNumber) {
		this.bedNumber = bedNumber;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
}
