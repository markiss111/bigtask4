package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Doctor;
import service.PrescriptionService;

public class ViewPatientPrescriptions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ViewPatientPrescriptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		Integer patientId = Integer.parseInt(request.getParameter("patientId"));
		String patientFullName = request.getParameter("patientFullName");
		request.setAttribute("patientFullName", patientFullName);
		Doctor doctor = (Doctor) request.getSession(true).getAttribute("user");
		if (doctor == null) {
			response.sendRedirect(request.getContextPath() + "/fail.jsp");
		} else {
			try {
				request.setAttribute("prescriptions", PrescriptionService.getPatientPrescriptionsByDoctorId(doctor.getId(), patientId));
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			request.getRequestDispatcher("patient_prescriptions.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
