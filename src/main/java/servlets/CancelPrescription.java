package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Prescription;
import service.PrescriptionService;

public class CancelPrescription extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CancelPrescription() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Integer prescriptionId = Integer.parseInt(request.getParameter("prescriptionId"));
		Prescription prescription = null;
		try {
			prescription = PrescriptionService.getPrescriptionById(prescriptionId);
			if (prescription.getType().equals("operation")) {
				PrescriptionService.deactivateOperationById(prescriptionId);
			} else if (prescription.getType().equals("treatment")) {
				PrescriptionService.deactivateTreatmentById(prescriptionId);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/success.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
