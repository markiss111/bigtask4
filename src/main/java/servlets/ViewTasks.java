package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Nurse;
import models.Prescription;
import service.PrescriptionService;


public class ViewTasks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ViewTasks() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		Nurse nurse = (Nurse) session.getAttribute("user");
		
		if (nurse == null) {
			response.sendRedirect(request.getContextPath() + "/fail.jsp");
		} else {
			request.setAttribute("nurse", nurse);
			try {
				Collection<Prescription> tasks = PrescriptionService.getCurrentDateActiveTreatmentsByNurseId(nurse.getId());
				session.setAttribute("tasks", tasks);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.getRequestDispatcher("tasks.jsp").forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
