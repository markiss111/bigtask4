package servlets;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.transfer.TransferManager;

import encryption.MD5;
import models.Doctor;
import service.LocationService;
import service.PatientService;

@MultipartConfig
public class PatientRegistration extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String S3_BUCKET_NAME = "arsen-hospital";
	private static final String S3_BUCKET_PATH = "https://s3.amazonaws.com/arsen-hospital/";
	private static final String DEFAULT_AVATAR_NAME = "default.png";
	private String avatar = "";
	private String avatarName = "";
	private String fname = "";
	private String lname = "";
	private String surname = "";
	private String phone = "";
	private String address = "";
	private String login = "";
	private String password = "";
	private Doctor doctor;
	private String diagnosis = "";
	
	private Integer locationNumbers = null;
	private Integer wardNumber = null;
	private Integer bedNumber = null;
	
	private File temp = null;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		doctor = (Doctor) request.getSession(true).getAttribute("user");
		//To prevent cached access
		if (doctor == null) {
			return;
		}
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();

		// Configure a repository (to ensure a secure temp location is used)
		ServletContext servletContext = this.getServletConfig().getServletContext();
		File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		factory.setRepository(repository);

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		try {
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = iter.next();

				if (item.isFormField()) {
					switch (item.getFieldName()) {
					case "fname":
						fname = item.getString("UTF-8");
						break;
					case "lname":
						lname = item.getString("UTF-8");
						break;
					case "surname":
						surname = item.getString("UTF-8");
						break;
					case "phone":
						phone = item.getString("UTF-8");
						break;
					case "address":
						address = item.getString("UTF-8");
						break;
					case "login":
						login = item.getString("UTF-8");
						break;
					case "password":
						password = MD5.md5(item.getString("UTF-8"));
						break;
					case "diagnosis":
						diagnosis = item.getString("UTF-8");
						break;
					case "locationId":
						locationNumbers = Integer.parseInt(item.getString("UTF-8"));
						break;
					default:
						break;
					}
				} else {
					temp = new File("temp.jpg");
					item.write(temp);
				}
			}
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if (PatientService.getPatientByLogin(login) != null) {
				request.setAttribute("patientFName", fname);
				request.setAttribute("patientLName", lname);
				request.setAttribute("patientSurname", surname);
				request.setAttribute("patientPhone", phone);
				request.setAttribute("patientAddress", address);
				request.setAttribute("patientDiagnosis", diagnosis);
				request.setAttribute("errorMsg", "Patient with such login already exists");
				request.getRequestDispatcher("/jsp/doctor/register_new_patient.jsp").forward(request, response);
			} else {
				wardNumber = locationNumbers/10;
				bedNumber = locationNumbers%10;
				
				uploadImageToS3(request, response);
				PatientService.insertNewPatient(fname, lname, surname, phone, address, doctor, diagnosis, login, password, avatar);
				LocationService.settlePatient(PatientService.getPatientByLogin(login).getId(), wardNumber, bedNumber);
				response.sendRedirect(request.getContextPath() + "/success.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

	private void uploadImageToS3(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		TransferManager tm = new TransferManager(new ProfileCredentialsProvider());
		// TransferManager processes all transfers asynchronously,
		// so this call will return immediately.
		if (temp.length() == 0) {
			avatar = S3_BUCKET_PATH + DEFAULT_AVATAR_NAME;
			return;
		}
		avatarName = "patient_" + login + ".jpg";
		avatar = S3_BUCKET_PATH + avatarName;
		tm.upload(S3_BUCKET_NAME, avatarName, temp);
	}
}