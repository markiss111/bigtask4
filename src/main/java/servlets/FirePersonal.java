package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DoctorService;
import service.NurseService;


public class FirePersonal extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FirePersonal() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Integer personalId = Integer.parseInt(request.getParameter("personalId"));
		String personalType = request.getParameter("personalType");
		
		if ("nurse".equals(personalType)) {
			try {
				NurseService.setNurseFiredById(personalId);
				response.sendRedirect(request.getContextPath() + "/success.jsp");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if ("doctor".equals(personalType)) {
			try {
				DoctorService.setDoctorFiredById(personalId);
				response.sendRedirect(request.getContextPath() + "/success.jsp");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
