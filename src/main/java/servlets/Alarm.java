package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Nurse;
import models.Patient;
import service.LocationService;
import sms.SMSCSender;

public class Alarm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Alarm() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		SMSCSender sender = new SMSCSender();
		Object user = request.getSession(true).getAttribute("user");
		Patient patient = null;
		Nurse nurse = null;
		
		if (user == null) {
			response.sendRedirect(request.getContextPath() + "/fail.jsp");
		}
		
		if (user instanceof Patient) {
			patient = (Patient) user;
			try {
				sender.sendSms(patient.getDoctor().getPhone(), "Alarm!!! Patient: " + patient.getFname() + " " + patient.getSurname() + ". Ward: " + LocationService.getLocationByPatientId(patient.getId()).getWardNumber(), 1, "", "", 0, "", "");
				response.sendRedirect(request.getContextPath() + "/alarm.jsp");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (user instanceof Nurse) {
			nurse = (Nurse) user;
			sender.sendSms("380635838088", "Alarm!!! Nurse: " + nurse.getFname() + " " + nurse.getSurname(), 1, "", "", 0, "", "");
			response.sendRedirect(request.getContextPath() + "/alarm.jsp");
		}	
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
