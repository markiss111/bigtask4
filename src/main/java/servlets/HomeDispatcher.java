package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Admin;
import models.Doctor;
import models.Nurse;
import models.Patient;
import models.Prescription;
import service.PrescriptionService;

public class HomeDispatcher extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public HomeDispatcher() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession(true);
		Object user = session.getAttribute("user");
		
		if (user instanceof Patient) {
			Collection<Prescription> prescriptions = null;
			try {
				prescriptions = PrescriptionService.getActivePatientPrescriptionsByCurrentDate(((Patient) user).getId());
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			session.setAttribute("prescriptions", prescriptions);
			response.sendRedirect(request.getContextPath() + "/jsp/patient/index.jsp");
		}
		else if (user instanceof Doctor) {
			Collection<Prescription> operations = null;
			try {
				operations = PrescriptionService.getActiveDoctorOperationsByCurrentDate(((Doctor)user).getId());
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			session.setAttribute("operations", operations);
			response.sendRedirect(request.getContextPath() + "/jsp/doctor/index.jsp");
		}
		else if (user instanceof Nurse) {
			try {
				Nurse nurse = (Nurse) user;
				Collection<Prescription> tasks = PrescriptionService.getCurrentDateActiveTreatmentsByNurseId(nurse.getId());
				session.setAttribute("tasks", tasks);
				session.setAttribute("nurse", nurse);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect(request.getContextPath() + "/jsp/nurse/index.jsp");
		}
		else if (user instanceof Admin) {
			response.sendRedirect(request.getContextPath() + "/jsp/admin/index.jsp");
		}
		else {
			response.sendRedirect(request.getContextPath());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
