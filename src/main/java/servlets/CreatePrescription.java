package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreatePrescription extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CreatePrescription() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String patientId = request.getParameter("patientId");
		String patientFullName = request.getParameter("patientFullName");
		
		request.setAttribute("patientId", patientId);
		request.setAttribute("patientFullName", patientFullName);
		request.getRequestDispatcher("create_prescription.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
