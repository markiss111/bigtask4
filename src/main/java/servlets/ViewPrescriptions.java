package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Patient;
import service.PrescriptionService;

public class ViewPrescriptions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ViewPrescriptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		
		Patient patient = (Patient) request.getSession(true).getAttribute("user");
		if (patient == null) {
			response.sendRedirect(request.getContextPath() + "/fail.jsp");
		} else {
			try {
				request.setAttribute("prescriptions", PrescriptionService.getActivePrescriptionsByPatientId(patient.getId()));
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			request.getRequestDispatcher("prescriptions.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
