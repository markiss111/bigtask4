package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Prescription;
import service.PrescriptionService;

public class ConfirmTask extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Hospital works from 6:00 till 22:00 (total - 16 hours)
	// But all the tasks must be performed by 22:00
	// So maximum time to appoint tasks is 21:00 (one hour just for performing existing tasks)
	private static final Long WORK_DAY_DURATION = (long) (15 * 60 * 60 * 1000);
	private static final Long DAY_DURATION = (long) (24 * 60 * 60 * 1000);
	private static final Integer MIN_HOUR = 6;
	private static final Integer MAX_HOUR = 21;
	
    public ConfirmTask() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		Integer taskId = Integer.parseInt(request.getParameter("taskId"));
		Prescription task = null;
		try {
			task = PrescriptionService.getPrescriptionById(taskId);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Long interval = WORK_DAY_DURATION/task.getFrequency();
		Date appointedDate = task.getAppointedDate();
		Date nextAppointedDate = new Date();
		nextAppointedDate.setTime(appointedDate.getTime() + interval);
		if (nextAppointedDate.getDay() != appointedDate.getDay() || nextAppointedDate.getHours() >= MAX_HOUR) {
			nextAppointedDate.setTime(appointedDate.getTime() + DAY_DURATION);
			nextAppointedDate.setHours(MIN_HOUR);
			nextAppointedDate.setMinutes(0);
		} 
		
		try {
			PrescriptionService.updateAppointedDateByPrescriptionId(nextAppointedDate, taskId);
			PrescriptionService.releasePrescriptionById(taskId);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect(request.getContextPath() + "/success.jsp");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
