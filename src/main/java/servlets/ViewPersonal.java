package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.AbstractPersonal;
import models.Doctor;
import models.Nurse;
import service.DoctorService;
import service.NurseService;

public class ViewPersonal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ViewPersonal() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
			try {
				Collection<Doctor> doctors = DoctorService.getAllDoctors();
				Collection<Nurse> nurses = NurseService.getAllNurses();
				List<AbstractPersonal> personal = new ArrayList<>();
				personal.addAll(doctors);
				personal.addAll(nurses);
				personal.sort(new Comparator<AbstractPersonal>() {
					@Override
					public int compare(AbstractPersonal o1, AbstractPersonal o2) {
						// TODO Auto-generated method stub
						return o1.getSurname().compareTo(o2.getSurname());
					}
				});
				request.setAttribute("personal", personal);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			request.getRequestDispatcher("personal.jsp").forward(request, response);
		
				
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
