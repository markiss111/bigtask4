package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Nurse;
import models.Prescription;
import service.PrescriptionService;


public class PerformTask extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public PerformTask() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		Integer taskId = Integer.parseInt(request.getParameter("taskId"));
		Nurse nurse = (Nurse) request.getSession(true).getAttribute("user");
		
		synchronized (PrescriptionService.class) {
			List<Prescription> tasks = (List<Prescription>) request.getSession(true).getAttribute("tasks");
			for (int i = 0; i < tasks.size(); i++){
				Prescription temp = tasks.get(i);
				Integer nurseId = 0;
				try {
					nurseId = PrescriptionService.getNurseIdByTaskId(temp.getId());
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (temp.getId() == taskId) {
					if (nurseId == 0) {
						try {
							PrescriptionService.setNurseByPrescriptionId(nurse.getId(), taskId);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						response.sendRedirect(request.getContextPath() + "/success.jsp");
					} else {
						response.sendRedirect(request.getContextPath() + "/jsp/nurse/fail.jsp");
					}
				}
			}
		}
		
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
