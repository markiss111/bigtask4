package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Doctor;
import models.Patient;
import service.PatientService;
import service.PrescriptionService;

public class AddPrescription extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AddPrescription() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Integer patientId = null;
		Patient patient = null;
		String type = "";
		Doctor doctor = null;
		String description = "";
		Integer frequency = null;
		Date startDate = null;
		Date finalDate = null;
		Date appointedDate = null;
		
		patientId = Integer.parseInt(request.getParameter("patientId"));
		try {
			patient = PatientService.getPatientById(patientId);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		type = request.getParameter("prescriptionType");
		doctor = (Doctor) request.getSession(true).getAttribute("user");
		if (doctor == null) {
			return;
		}
		
		description = request.getParameter("description");
		
		if (type.equals("treatment")) {
			frequency = Integer.parseInt(request.getParameter("frequency"));
			try {
				startDate = new Date();
				finalDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("final_date"));
				appointedDate = new Date();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (type.equals("operation")) {
			frequency = 0;
			startDate = null;
			finalDate = null;
			try {
				appointedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(request.getParameter("appointed_date"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			PrescriptionService.addPrescription(patient, type, doctor, description, frequency, startDate, finalDate, appointedDate);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect(request.getContextPath() + "/success.jsp");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
