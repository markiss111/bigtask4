package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Doctor;
import service.PrescriptionService;

public class ViewOperations extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ViewOperations() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Doctor doctor = (Doctor) request.getSession(true).getAttribute("user");
		if (doctor == null) {
			response.sendRedirect(request.getContextPath() + "/fail.jsp");
		} else {
			try {
				request.setAttribute("operations", PrescriptionService.getAllOperationsByDoctorId(doctor.getId()));
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			request.getRequestDispatcher("operations.jsp").forward(request, response);
		}
				
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
