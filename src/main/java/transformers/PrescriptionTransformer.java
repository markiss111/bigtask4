package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import models.Doctor;
import models.Nurse;
import models.Patient;
import models.Prescription;
import service.DoctorService;
import service.NurseService;
import service.PatientService;



public abstract class PrescriptionTransformer {

    public static Collection<Prescription> fromRSToCollection(ResultSet resultSet) throws SQLException, ClassNotFoundException{
    	Collection<Prescription> prescriptions = new ArrayList<Prescription>();
        
    	while (resultSet.next()){
    		Integer id = resultSet.getInt("prescription_id");
            Patient patient = PatientService.getPatientById(resultSet.getInt("patient_id"));
            String type = resultSet.getString("type");
            Doctor doctor = DoctorService.getDoctorById(resultSet.getInt("doctor_id"));
            String description = resultSet.getString("description");
            Integer frequency = resultSet.getInt("frequency");
            Date startDate = resultSet.getDate("start_date");
            Date finalDate = resultSet.getDate("final_date");
            Date appointedDate = resultSet.getTimestamp("appointed_date");
            Boolean isActive = resultSet.getBoolean("is_active");
            Nurse nurse = NurseService.getNurseById(resultSet.getInt("nurse_id"));
			
			prescriptions.add(new Prescription(id, patient, type, doctor, description, frequency, startDate, finalDate, appointedDate, isActive, nurse));
        }
    	
        return prescriptions;
    }
    
    public static Prescription fromRSToPrescription(ResultSet resultSet) throws SQLException, ClassNotFoundException{
        
    	if (resultSet.next()){
    		Integer id = resultSet.getInt("prescription_id");
            Patient patient = PatientService.getPatientById(resultSet.getInt("patient_id"));
            String type = resultSet.getString("type");
            Doctor doctor = DoctorService.getDoctorById(resultSet.getInt("doctor_id"));
            String description = resultSet.getString("description");
            Integer frequency = resultSet.getInt("frequency");
            Date startDate = resultSet.getDate("start_date");
            Date finalDate = resultSet.getDate("final_date");
            Date appointedDate = resultSet.getTimestamp("appointed_date");
            Boolean isActive = resultSet.getBoolean("is_active");
            Nurse nurse = NurseService.getNurseById(resultSet.getInt("nurse_id"));
			
			return new Prescription(id, patient, type, doctor, description, frequency, startDate, finalDate, appointedDate, isActive, nurse);
        }
    	
        return null;
    }
}
