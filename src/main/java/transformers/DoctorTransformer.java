package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Doctor;

public abstract class DoctorTransformer {

    public static Collection<Doctor> fromRSToCollection(ResultSet resultSet) throws SQLException{
    	Collection<Doctor> doctors = new ArrayList<Doctor>();
        
    	while (resultSet.next()){
    		Integer id = resultSet.getInt("doctor_id");
            String fname = resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String type = resultSet.getString("type");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String avatar = resultSet.getString("avatar");
            Boolean isPresent = resultSet.getBoolean("is_present");
            
            doctors.add(new Doctor(id, fname, lname, surname, type, phone, address, login, password, avatar, isPresent));
        }
    	
        return doctors;
    }
    
    public static Doctor fromRSToDoctor(ResultSet resultSet) throws SQLException{
        
    	if (resultSet.next()){
    		Integer id = resultSet.getInt("doctor_id");
            String fname =resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String type = resultSet.getString("type");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String avatar = resultSet.getString("avatar");
            Boolean isPresent = resultSet.getBoolean("is_present");
            
            return new Doctor(id, fname, lname, surname, type, phone, address, login, password, avatar, isPresent);
        }
    	
        return null;
    }
}
