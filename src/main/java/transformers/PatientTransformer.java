package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import models.Doctor;
import models.Patient;
import service.DoctorService;



public abstract class PatientTransformer {

    public static Collection<Patient> fromRSToCollection(ResultSet resultSet) throws SQLException, ClassNotFoundException{
    	Collection<Patient> patients = new ArrayList<Patient>();
        
    	while (resultSet.next()){
    		Integer id = resultSet.getInt("patient_id");
            String fname = resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            Doctor doctor = DoctorService.getDoctorById(resultSet.getInt("doctor_id"));
            String diagnosis = resultSet.getString("diagnosis");
            Date enterDate = resultSet.getDate("enter_date");
            Date dischargeDate = resultSet.getDate("discharge_date");
            String login = resultSet.getString("login");
			String password = resultSet.getString("password");
			String avatar = resultSet.getString("avatar");
			
			patients.add(new Patient(id, fname, lname, surname, phone, address, doctor, diagnosis, enterDate, dischargeDate, login, password, avatar));
        }
    	
        return patients;
    }
    
    public static Patient fromRSToPatient(ResultSet resultSet) throws SQLException, ClassNotFoundException{
        
    	if (resultSet.next()){
    		Integer id = resultSet.getInt("patient_id");
            String fname = resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            Doctor doctor = DoctorService.getDoctorById(resultSet.getInt("doctor_id"));
            String diagnosis = resultSet.getString("diagnosis");
            Date enterDate = resultSet.getDate("enter_date");
            Date dischargeDate = resultSet.getDate("discharge_date");
            String login = resultSet.getString("login");
			String password = resultSet.getString("password");
			String avatar = resultSet.getString("avatar");
			
			return new Patient(id, fname, lname, surname, phone, address, doctor, diagnosis, enterDate, dischargeDate, login, password, avatar);
        }
    	
        return null;
    }
}
