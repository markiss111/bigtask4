package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Admin;



public abstract class AdminTransformer {

    public static Collection<Admin> fromRSToCollection(ResultSet resultSet) throws SQLException{
    	Collection<Admin> admins = new ArrayList<Admin>();
        
    	while (resultSet.next()){
            String fname =resultSet.getString("fname");
            String surname = resultSet.getString("surname");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            
            admins.add(new Admin(fname, surname, login, password));
        }
    	
        return admins;
    }
    
    public static Admin fromRSToAdmin(ResultSet resultSet) throws SQLException{
        
    	if (resultSet.next()){
    		String fname =resultSet.getString("fname");
            String surname = resultSet.getString("surname");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            
            return new Admin(fname, surname, login, password);
        }
    	
        return null;
    }
}
