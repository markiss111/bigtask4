package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Location;
import models.Patient;
import service.PatientService;

public class LocationTransfromer {
	public static Collection<Location> fromRSToCollection(ResultSet resultSet) throws SQLException, ClassNotFoundException{
    	Collection<Location> locations = new ArrayList<Location>();
        
    	while (resultSet.next()){
    		Integer wardNumber = resultSet.getInt("ward_number");
    		Integer bedNumber = resultSet.getInt("bed_number");
    		Patient patient = PatientService.getPatientById(resultSet.getInt("patient_id"));
            
            
            locations.add(new Location(wardNumber, bedNumber, patient));
        }
    	
        return locations;
    }
    
    public static Location fromRSToLocation(ResultSet resultSet) throws SQLException, ClassNotFoundException{
        
    	while (resultSet.next()){
    		Integer wardNumber = resultSet.getInt("ward_number");
    		Integer bedNumber = resultSet.getInt("bed_number");
    		Patient patient = PatientService.getPatientById(resultSet.getInt("patient_id"));
    		
            return new Location(wardNumber, bedNumber, patient);
        }
    	
        return null;
    }
}
