package transformers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import models.Nurse;



public abstract class NurseTransformer {

    public static Collection<Nurse> fromRSToCollection(ResultSet resultSet) throws SQLException{
    	Collection<Nurse> nurses = new ArrayList<Nurse>();
        
    	while (resultSet.next()){
    		Integer id = resultSet.getInt("nurse_id");
            String fname = resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String avatar = resultSet.getString("avatar");
            Boolean isPresent = resultSet.getBoolean("is_present");
            
            nurses.add(new Nurse(id, fname, lname, surname, phone, address, login, password, avatar, isPresent));
        }
    	
        return nurses;
    }
    
    public static Nurse fromRSToNurse(ResultSet resultSet) throws SQLException{
        
    	if (resultSet.next()){
    		Integer id = resultSet.getInt("nurse_id");
            String fname =resultSet.getString("fname");
            String lname = resultSet.getString("lname");
            String surname = resultSet.getString("surname");
            String phone = resultSet.getString("phone");
            String address = resultSet.getString("address");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String avatar = resultSet.getString("avatar");
            Boolean isPresent = resultSet.getBoolean("is_present");
            
            return new Nurse(id, fname, lname, surname, phone, address, login, password, avatar, isPresent);
        }
    	
        return null;
    }
}
