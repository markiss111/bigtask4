package persistant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public abstract class ConnectionManager {
    private static Connection connection = null;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
    	Class.forName("com.mysql.jdbc.Driver");
        String jdbcUrl = "jdbc:mysql://localhost:3306/hospital?user=root&password=root&useSSL=false&useUnicode=true&characterEncoding=UTF8";

        if (connection == null){
            connection = DriverManager.getConnection(jdbcUrl);
        }

        return connection;
    }
}
