<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="footer2">
	<a href="${pageContext.request.contextPath}"><img
		src="${pageContext.request.contextPath}/assets/img/logo/heartify-logo-lite.png"
		alt="" /></a>
</div>

<!-- Footer Section  -->
<div class="footer2-bottom">
	<div class="container">
		<div class="col-md-6">
			<p>
				Copyright 2016. <a href="${pageContext.request.contextPath}"><b>HEARTIFY</b></a>. All Rights Reserved.
			</p>
		</div>
		<div class="col-md-6">
			<ul class="footer-social">
				<li><a href="http://www.facebook.com/arsen.nykytenko" target="_blank"><i
						class="fa fa-facebook"></i></a></li>
				<li><a href="https://twitter.com/markiss111" target="_blank"><i
						class="fa fa-twitter"></i></a></li>
				<li><a href="https://plus.google.com/u/0/112064597780930329836"
					target="_blank"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="https://new.vk.com/relaxable" target="_blank"><i
						class="fa fa-vk"></i></a></li>
			</ul>
		</div>
		<a href="javascript:void(0)" class="bttop"><img
			src="${pageContext.request.contextPath}/assets/img/backtotop.jpg"
			alt="Back To Top" /></a>
	</div>
</div>
<!-- Footer Section  -->