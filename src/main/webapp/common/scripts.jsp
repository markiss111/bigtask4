<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/assets/js/jquery-1.9.1.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/layerslider/js/greensock.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="https://use.fontawesome.com/ab02bc411f.js"></script>
<script>
	jQuery('.bttop').click(function(){
		jQuery('html, body').animate({scrollTop:0}, 'slow');
	});
</script>
<script src="${pageContext.request.contextPath}/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/isotope/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/waypoint-sticky-nav/waypoints.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/waypoint-sticky-nav/waypoints-sticky.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/custom-scripts.js"></script>