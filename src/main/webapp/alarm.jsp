<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor - Register New Patient</title>
<link rel="stylesheet"
	href="${request.getContextPath()}/Hospital/assets/css/style_form.css">
</head>
<body>
	<div class="container">
		<div class="logo">
			<a href="${request.getContextPath()}/Hospital/"><img
				src="${request.getContextPath()}/Hospital/assets/img/logo/heartify-logo.png"
				alt="Heartify logo" width="141" height="84"></a>
				<br>
				<br>
				<br>
			<h1 style="font-size: 28px;">Hold On! The help is coming!</h1>
			<br>
			<br>
		<img alt="Success" src="${request.getContextPath()}/Hospital/assets/img/success.png" width="150" height="150">
		</div>
			<div class="button-container">
				<a href="${request.getContextPath()}/Hospital/home"><button type="button" class="button">
					<span>Home</span>
				</button>
				</a>
			</div>
		<br>
	</div>
</body>
</html>