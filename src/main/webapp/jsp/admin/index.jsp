<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>

<!-- Meta -->
<meta charset="utf-8">
<title>Admin - Home</title>

<jsp:include page="${request.contextPath}/common/styles.jsp"></jsp:include>

</head>

<body>
	<jsp:include page="admin_header.jsp"></jsp:include>
	<!-- Insurance Section -->
	<section id="insurance-types">
		<div class="container">
			<div class="row">
				<div
					class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<div class="insurance-stat text-center">
						<h2>Hello, ${user.fname}</h2>
						<h5>How are you today?</h5>
					</div>
				</div>
				<div class="insurance-information">
					<div class="col-xs-12 col-sm-12 col-md-offset-2 col-lg-offset-0 col-md-4 col-lg-4">
						<div class="center">
							<h5>Have some work?</h5>
						</div>
						<div class="button-container">
							<a href="add_new_personal.jsp"><button type="button"
									class="button">
									<span>Add New Personal</span>
								</button></a>
						</div>
						<div class="button-container">
							<a href="view_personal">
								<button type="button" class="button">
									<span>View Personal</span>
								</button>
							</a>
						</div>
						<div class="button-container">
							<a href="${request.getContextPath()}/Hospital/logout">
								<button type="button" class="button green2-butt">
									<span>Log Out</span>
								</button>
							</a>
						</div>
					</div>
					<div class="visible-lg visible-md col-md-4 col-lg-4">
						<img id="doc" class="pull-left"
							src="${pageContext.request.contextPath}/assets/img/general/admin.png"
							alt="Admin" />
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="${request.contextPath}/common/footer.jsp"></jsp:include>
	<jsp:include page="${request.contextPath}/common/scripts.jsp"></jsp:include>
</body>

</html>