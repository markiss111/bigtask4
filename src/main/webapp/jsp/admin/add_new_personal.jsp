<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin - Add New Personal</title>
<link rel="stylesheet"
	href="${request.getContextPath()}/Hospital/assets/css/style_form.css">
	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
	<div class="container">
		<div class="logo">
			<a href="${request.getContextPath()}/Hospital/"><img
				src="${request.getContextPath()}/Hospital/assets/img/logo/heartify-logo.png"
				alt="Heartify logo" width="141" height="84"></a>
		</div>
		<form id="add_pers" action="registration" method="post"
			enctype="multipart/form-data">
			<h1>New Personal</h1>
			<div class="form-radio">
				<div class="radio">
					<label><input type="radio" name="personalType"
						value="nurse" id="nurse" required /><i class="helper"></i>Nurse
					</label>
				</div>
				<div class="radio">
					<label><input type="radio" name="personalType"
						value="doctor" id="doctor" required /><i class="helper"></i>Doctor
					</label>
				</div>
			</div>
			<div class="form-group">
				<input name="fname" type="text" required value="${personalFName}" /><label for="input"
					class="control-label">First Name</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="lname" type="text" required value="${personalLName}" /> <label for="input"
					class="control-label">Last Name</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="surname" type="text" required value="${personalSurname}" /> <label for="input"
					class="control-label">Surname</label><i class="bar"></i>
			</div>
			<div class="form-group hide">
				<input id="doctor_type" name="doctorType" type="text" value="${doctorType}" /> <label for="input"
					class="control-label">Specialization</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="phone" type="tel" required value="${personalPhone}" /> <label for="input"
					class="control-label">Phone</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="address" type="text" required value="${personalAddress}" /> <label for="input"
					class="control-label">Address</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="login" type="text" required /> <label for="input"
					class="control-label">Login</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="password" type="password" required /> <label
					for="input" class="control-label">Password</label><i class="bar"></i>
			</div>
			<div class="box" style="text-align: center;">
				<input type="file" name="file-7[]" id="file-7"
					class="inputfile inputfile-6"
					data-multiple-caption="{count} files selected" multiple /> <label
					for="file-7"><span>Avatar...</span> <strong><svg
							xmlns="http://www.w3.org/2000/svg" width="20" height="17"
							viewBox="0 0 20 17">
							<path
								d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
						Choose a file&hellip;</strong></label>
			</div>
			<div class="button-container">
				<button type="submit" class="button">
					<span>Register</span>
				</button>
				<div class="error">
					<p>${errorMsg}</p>
				</div>
			</div>
		</form>
		<br>
	</div>
	<jsp:include page="${request.getContextPath()}/common/scripts.jsp"></jsp:include>
</body>
</html>