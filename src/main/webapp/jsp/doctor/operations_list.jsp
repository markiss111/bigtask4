<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tld/oper.tld" prefix="my"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<section id="team" class="gray oper">
	<div class="container">
		<hr>
		<div class="row">
			<div id="container" class="container team-detail">
				<c:forEach var="currentOper" items="${operations}">
					<my:oper operation="${currentOper}" />
				</c:forEach>
			</div>
		</div>
		<hr>
	</div>
</section>