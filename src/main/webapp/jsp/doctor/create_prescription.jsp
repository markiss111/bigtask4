<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor - New Prescription</title>
<link rel="stylesheet"
	href="${request.getContextPath()}/Hospital/assets/css/style_form.css">
</head>
<body>
	<div class="container">
		<div class="logo">
			<a href="${request.getContextPath()}/Hospital/"><img
				src="${request.getContextPath()}/Hospital/assets/img/logo/heartify-logo.png"
				alt="Heartify logo" width="141" height="84"></a> <br>
			<br>
			<br>
			<h1>${patientFullName}</h1>
			<p style="color: #999">New Prescription</p>
		</div>

		<form id="add_prescr" action="addPrescription" method="post">
			<input type="hidden" name="patientId" value="${patientId}" />
			<div class="form-radio">
				<div class="radio">
					<label><input type="radio" name="prescriptionType"
						value="treatment" id="treatment" required /><i class="helper"></i>Treatment
					</label>
				</div>
				<div class="radio">
					<label><input type="radio" name="prescriptionType"
						value="operation" id="operation" required /><i class="helper"></i>Operation
					</label>
				</div>
			</div>
			<div class="form-group">
				<input name="description" type="text" required /><label for="input"
					class="control-label">Description</label><i class="bar"></i>
			</div>
			<div class="form-group hide one">
				<input name="frequency" type="number" id="frequency" /><label
					for="input" class="control-label">Frequency (per day)</label><i class="bar"></i>
			</div>
			<div class="form-group hide one">
				<input name="final_date" type="date" id="final_date" /><label
					for="input" class="control-label">End of Receivng</label><i
					class="bar"></i>
			</div>
			<div class="form-group hide two">
				<input name="appointed_date" id="appointed_date"
					type="datetime-local" /><label for="input" class="control-label">Appointed
					Date</label><i class="bar"></i>
			</div>

			<div class="button-container">
				<button type="submit" class="button">
					<span>Prescribe</span>
				</button>
			</div>
		</form>
		<br>
	</div>
	<jsp:include page="${request.getContextPath()}/common/scripts.jsp"></jsp:include>
</body>
</html>