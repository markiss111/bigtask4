<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tld/up_prescr.tld" prefix="my"%>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>

<!-- Meta -->
<meta charset="utf-8">
<title>Patient - Home</title>

<jsp:include page="${request.contextPath}/common/styles.jsp"></jsp:include>

</head>

<body>
	<jsp:include page="patient_header.jsp"></jsp:include>
	<!-- Insurance Section -->
	<section id="insurance-types">
		<div class="container">
			<div class="row">
				<div
					class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<div class="insurance-stat text-center">
						<h2>Hello, ${user.fname}</h2>
						<h5>How do you feel today?</h5>
					</div>
				</div>
				<div class="insurance-information">
					<div
						class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div class="center">
							<h5>Want to check something?</h5>
						</div>
						<div class="button-container">
							<a href="view_prescriptions"><button type="button"
									class="button">
									<span>My Prescriptions</span>
								</button></a>
						</div>
						<div class="button-container">
							<a href="${request.getContextPath()}/Hospital/logout">
								<button type="button" class="button">
									<span>Log Out</span>
								</button>
							</a>
						</div>
						<div class="button-container">
							<a href="${request.getContextPath()}/Hospital/alarm">
								<button type="button" class="button red-butt">
									<span><strong>ALARM!</strong></span>
								</button>
							</a>
						</div>
					</div>
					<div class="visible-lg visible-md col-md-4 col-lg-4">
						<img id="doc" class="pull-left"
							src="${pageContext.request.contextPath}/assets/img/general/patient.png"
							alt="Patient" />
					</div>
					<!-- Schedule slider -->
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div id="time-plan" class="owl-carousel owl-theme">
							<div class="item sidebox-timing">
								<h2>Today</h2>
								<h6>Upcoming Prescriptions</h6>
								<p>Your 5 upcoming prescriptions</p>
								<ul class="list-unstyled timings">
									<my:upPrescr prescriptions="${prescriptions}" />
								</ul>
							</div>
						</div>
						<!--  timeplan div ends -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="${request.contextPath}/common/footer.jsp"></jsp:include>
	<jsp:include page="${request.contextPath}/common/scripts.jsp"></jsp:include>
</body>

</html>