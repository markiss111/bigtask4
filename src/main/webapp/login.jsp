<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Log In</title>
<link href="assets/css/style_form.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="container">
		<div class="logo">
			<a href="${request.getContextPath()}/Hospital/"><img src="assets/img/logo/heartify-logo.png" alt="Heartify logo"
				width="141" height="84"></a>
		</div>
		<form action="loginAction" method="post">
			<h1>Log In</h1>
			<div class="form-group">
				<input name="login" type="text" required value="${userLogin}" /><label
					for="input" class="control-label">Login</label><i class="bar"></i>
			</div>
			<div class="form-group">
				<input name="password" type="password" required /> <label
					for="input" class="control-label">Password</label><i class="bar"></i>
			</div>
			<div class="form-radio">
				<div class="radio">
					<label> <input type="radio" name="user" value="doctor" required /><i
						class="helper"></i>I'm a Doctor
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="user" value="nurse" required /><i
						class="helper"></i>I'm a Nurse
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="user" value="patient" required /><i
						class="helper"></i>I'm a Patient
					</label>
				</div>
				<div class="radio">
					<label> <input type="radio" name="user" value="admin" required /><i
						class="helper"></i>I'm an Admin
					</label>
				</div>
			</div>
			<div class="button-container">
				<button type="submit" class="button">
					<span>Log In</span>
				</button>
				<div class="error">
					<p>${errorMsg}</p>
				</div>
			</div>
		</form>
		<br>
	</div>
</body>
</html>